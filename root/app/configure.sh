#! /bin/sh

cat /app/config.json | sed \
  -e "s/{{BRIDGE_USERNAME}}/${BRIDGE_USERNAME}/g" \
  -e "s/{{BRIDGE_PIN}}/${BRIDGE_PIN}/g" \
  -e "s/{{BRIDGE_SETUPID}}/${BRIDGE_SETUPID}/g" \
  -e "s/{{EMAIL}}/${EMAIL}/g" \
  -e "s/{{PASSWORD}}/${PASSWORD}/g" \
  -e "s/{{MFA}}/${MFA}/g" \
  > /app/homebridge/config.json
